# frozen string_literals: true
require_relative '../helpers/bowling'
require_relative '../helpers/array_helpers'


# This claass receives a score per player and calculates the total score of the game.
# [{player: 'Player 1', score: [{frame: 1, score: 10}, {frame: 2, score: 5}, {frame: 2, score: 4}]}]
class ScoreBoard
  def initialize(total_frames: 10, total_pins: 10, shots_per_frame: 2, final_frame_shots: 3, score: [])
    @total_frames = total_frames
    @total_pins = total_pins
    @shots_per_frame = shots_per_frame
    @final_frame_shots = final_frame_shots
    @score = score
    @score_board = []
    @pinfalls_str = 'Pinfalls  '
    @total_score =  'Score     '
  end

  # Return the scoreboard

  def compute_scoreboard
    @score_board.push(number_of_frames_line)
    scores_to_print
  end

  def print_scoreboard
    puts @score_board
    @score_board
  end

  private

  def number_of_frames_line
    frame_str = 'Frame     '
    @total_frames.times do |frame|
      frame_str += "#{frame + 1}       "
    end
    frame_str
  end

  def scores_to_print
    @score.each_key do |player|
      @score_board.push(player)
      @score[player].each do |score|
        # Put player name in scoreboard
        # Compute pinfalls
        handle_pinfalls(score[:pinfalls])
        handle_total_score(score[:total_score])
      end
      # Save pinfalls then reset
      @score_board.push(@pinfalls_str)
      @pinfalls_str = 'Pinfalls  '
      @score_board.push(@total_score)
      @total_score = 'Score     '
    end
  end

  def handle_pinfalls(pinfalls)
    pinfalls_size = pinfalls.size
    pinfalls_size.times do |i|
      if Bowling.strike?(pinfalls[i])
        @pinfalls_str += '  X  '
      elsif i < pinfalls_size - 1 && Bowling.spare?(pinfalls[i], pinfalls[i + 1])
        @pinfalls_str += "#{pinfalls[i]}  /  "
        break unless pinfalls_size == @final_frame_shots
      else
        @pinfalls_str += " #{pinfalls[i]}  "
      end
    end
    @pinfalls_str += '| '
  end

  def handle_total_score(total_score)
    @total_score += "#{total_score}      "
  end

  def handle_regular_pinfalls(pinfalls)
    pinfalls.each do |pinfall|
      @pinfalls_str += "#{pinfall}  "
    end
    @pinfalls_str += '| '
  end
end
