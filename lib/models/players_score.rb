# Receives an array with the players and their scores and returns it organized by frame and shot.
# Returns [{playerName: [{frame: 1, score: 10}, {frame: 2, score: 5}, {frame: 2, score: 4}]}]
require_relative '../helpers/bowling'
require_relative '../helpers/array_helpers'


class PlayersScore
  attr_reader :players_scores, :game

  def initialize(total_frames: 10, total_pins: 10, shots_per_frame: 2, final_frame_shots: 3, game: [])
    @total_frames = total_frames
    @total_pins = total_pins
    @shots_per_frame = shots_per_frame
    @final_frame_shots = final_frame_shots
    # Group by player
    @game = game.group_by { |obj| obj[:player] }
    @players_scores = {}
    @skip_next_shot_iter = false
  end

  def compute_players_score
    @game.each_key do |player|
      # Frame starts at 1
      frame = 1
      @game[player].each_with_index do |shot, index|
        # Skip if we already computed the score for this frame
        if @skip_next_shot_iter
          @skip_next_shot_iter = false
          next
        end

        # If it's the last frame we need to check the number of shots in the last frame
        if frame == @total_frames
          # Use the final_frame_shots to compute the number of shots
          final_shots = get_pinfalls_score(player, index, @final_frame_shots)
          populate_players_pinfalls(player, { frame: frame, pinfalls: final_shots })

          break
        # If it's a strike, we need to make sure it's computed only by itself
        elsif shot[:pinfalls] == @total_pins
          populate_players_pinfalls(player, { frame: frame, pinfalls: [shot[:pinfalls]] })
        else
          pinfalls_shot = get_pinfalls_score(player, index, @shots_per_frame)
          populate_players_pinfalls(player, { frame: frame, pinfalls: pinfalls_shot })
          @skip_next_shot_iter = true
        end

        frame = frame.next
      end
    end
    compute_total_score
  end

  private

  def populate_players_pinfalls(player, score_obj)
    @players_scores[player] = [] if @players_scores[player].nil?
    @players_scores[player].push(score_obj)
  end

  def get_pinfalls_score(player, index, size)
    @game[player][index..index + size - 1].map { |obj| obj[:pinfalls] }
  end

  def compute_total_score
    @players_scores.each_key do |player|
      @players_scores[player].each_with_index do |curr_frame, index|
        prev_frame = index.zero? ? nil : @players_scores[player][index - 1]
        next_frame = @players_scores[player][index + 1]
        if Bowling.strike?(curr_frame[:pinfalls].first) && index != @players_scores[player].size - 1
          compute_strike_score(next_frame, index, curr_frame, prev_frame, player)
        elsif Bowling.spare?(curr_frame[:pinfalls].first, curr_frame[:pinfalls].last) && index != @players_scores[player].size - 1
          # In case of spare only the first pinfall is added to the total curr_frame
          compute_spare_score(next_frame, curr_frame, prev_frame)
        else
          puts curr_frame
          compute_regular_score(prev_frame, curr_frame)
        end
      end
    end
  end

  def compute_strike_score(next_frame, index, curr_frame, prev_frame, player)
    value_to_add =
      if Bowling.strike?(next_frame[:pinfalls].first) && index + 2 <= @players_scores[player].size - 1
        curr_frame[:pinfalls].first + @players_scores[player][index + 2][:pinfalls].first
      else
        curr_frame[:pinfalls].reduce(:+)
      end
    calculate_frame_total_score(
      prev_frame,
      curr_frame,
      sum_frame_pinfalls(next_frame) + value_to_add
    )
  end

  def compute_spare_score(next_frame, curr_frame, prev_frame)
    calculate_frame_total_score(
      prev_frame,
      curr_frame,
      sum_frame_pinfalls(curr_frame) + next_frame[:pinfalls].first
    )
  end

  def compute_regular_score(prev_frame, curr_frame)
    last_frame = curr_frame[:frame] == @total_frames
    calculate_frame_total_score(prev_frame, curr_frame, sum_frame_pinfalls(curr_frame, last_frame))
  end

  def sum_frame_pinfalls(frame, last_frame = false)
    unless frame[:frame] == @total_frames && !last_frame
      frame[:pinfalls].reduce(:+)
    else
      frame[:pinfalls].reduce(:+) - frame[:pinfalls].last
    end
  end

  def calculate_frame_total_score(prev_frame, curr_frame, value_to_add)
    curr_frame[:total_score] =
      if prev_frame
        prev_frame[:total_score] + value_to_add
      else
        value_to_add
      end
  end
end
