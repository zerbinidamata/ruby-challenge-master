require 'dotenv/load'

require_relative './models/players_score'
require_relative './models/score_board'

# Main class
class Main
  # Number of frames to show in the scoreboard
  TOTAL_FRAMES = ENV['TOTAL_FRAMES'] || 10
  # Number of pins to show in the scoreboard
  TOTAL_PINS = ENV['TOTAL_PINS'] || 10

  SHOTS_PER_FRAME = ENV['SHOTS_PER_FRAME'] || 2
  FINAL_FRAME_SHOTS = ENV['FINAL_FRAME_SHOTS'] || 3

  def initialize(game)
    @game = game
  end

  def compute_score
    players_score = PlayersScore.new(
      total_frames: TOTAL_FRAMES,
      total_pins: TOTAL_PINS,
      shots_per_frame: SHOTS_PER_FRAME,
      final_frame_shots: FINAL_FRAME_SHOTS,
      game: @game
    )
    score = players_score.compute_players_score
    scoreboard = ScoreBoard.new(
      total_frames: TOTAL_FRAMES,
      total_pins: TOTAL_PINS,
      shots_per_frame: SHOTS_PER_FRAME,
      final_frame_shots: FINAL_FRAME_SHOTS,
      score: score
    )
    scoreboard.compute_scoreboard
    scoreboard.print_scoreboard
  end
end
