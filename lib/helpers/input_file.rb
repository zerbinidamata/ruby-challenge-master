# Parse Input File
class InputFile
  attr_reader :file_path

  def initialize(file_path)
    puts "Initializing InputFile for file #{file_path}"

    raise 'File not found' unless File.exist?(file_path)

    @file_path = file_path
  end

  def parse
    raw_data = []
    total_readed_lines = 0

    File.open(@file_path, 'r') do |file|
      file.each_line do |line|
        total_readed_lines += 1
        spllited_line = line.split

        raise 'Invalid line' unless validate_line(spllited_line)

        raise 'Invalid pinfalls' unless validate_pinfalls(spllited_line[1])

        raw_data << {
          player: spllited_line[0], pinfalls: spllited_line[1].to_i
        }
      end
    end
    puts "Readed #{total_readed_lines} lines from file #{@file_path}"
    raw_data
  end

  private

  # If line has more than 2 elements, it's not valid
  def validate_line(line)
    line.size == 2
  end

  # If the pinfalls is not numeric, it's not valid unless it's a `F`, which is converted to 0
  def validate_pinfalls(pinfalls)
    # Zero is not positive
    (numeric?(pinfalls) && (pinfalls.to_i.positive? || pinfalls.to_i.zero?)) || pinfalls == 'F'
  end

  def numeric?(obj)
    !obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/).nil?
  end
end
