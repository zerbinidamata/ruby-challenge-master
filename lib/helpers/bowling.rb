
# Bowling helpers
class Bowling
  TOTAL_PINS = ENV['TOTAL_PINS'] || 10

  # Check if it's a strike, usefull to return X for strikes
  def self.strike?(score)
    score == TOTAL_PINS
  end

  # Check if it's a spare, usefull to return / for spares
  def self.spare?(prev_score, curr_score)
    prev_score + curr_score == TOTAL_PINS
  end

  

end
