require 'helpers/input_file'

RSpec.describe InputFile do
  it 'initialize with a file path' do
    file_path = 'input.txt'
    input_file = described_class.new(file_path)
    expect(input_file.file_path).to eq(file_path)
  end

  it 'raise an error if file not found' do
    file_path = 'not_exists.txt'
    expect { described_class.new(file_path) }.to raise_error('File not found')
  end

  it 'parse the file' do
    file_path = 'input.txt'
    input_file = described_class.new(file_path)
    # Just check if the file is parsed by making sure the keys are present
    expect(input_file.parse).to include({ player: 'Jeff', pinfalls: 10 })
  end

  it 'does not parse the file if it is not valid' do
    file_path = 'spec/fixtures/negative/free-text.txt'
    input_file = described_class.new(file_path)
    expect { input_file.parse }.to raise_error('Invalid line')
  end

  it 'does not parse the file if score is not numeric' do
    file_path = 'spec/fixtures/negative/invalid-score.txt'
    input_file = described_class.new(file_path)
    expect { input_file.parse }.to raise_error('Invalid pinfalls')
  end

  it 'does not parse the file if score is negative' do
    file_path = 'spec/fixtures/negative/negative.txt'
    input_file = described_class.new(file_path)
    expect { input_file.parse }.to raise_error('Invalid pinfalls')
  end
end
