require 'spec_helper'
require 'main'

RSpec.describe Main do
  let(:perfect) { file_fixture('perfect.txt') }
  let(:scores) { file_fixture('scores.txt') }
  let(:invalid) { file_fixture('invalid-score.txt') }
  let(:free_text) { file_fixture('free-text.txt') }
  let(:negative) { file_fixture('negative.txt') }
  let(:all_fouls) { file_fixture('all-fouls.txt') }



  context 'when input file is valid' do
    context 'with more than two players' do
      before do
        input_file = InputFile.new(scores)
        game = input_file.parse
        @main = described_class.new(game)
      end

      it 'prints the game scoreboard to stdout' do
        expect { @main.compute_score }.to output(/Jeff/).to_stdout
        expect { @main.compute_score }.to output(/John/).to_stdout
      end
    end

    context 'with strikes in all throwings' do
      before do
        input_file = InputFile.new(perfect)
        game = input_file.parse
        @main = described_class.new(game)
      end

      it 'prints a perfect game scoreboard' do
        # Perfect score is 300
        expect { @main.compute_score }.to output(/300/).to_stdout
      end
    end

    context 'with fouls in all throwings' do
      it 'prints the game scoreboard to stdout' do
        input_file = InputFile.new(all_fouls)
        game = input_file.parse
        @main = described_class.new(game)
        expect { @main.compute_score }.to output(/0/).to_stdout
      end
    end
  end

  context 'when input file is invalid' do
    context 'with invalid characters present' do
      it 'raises the corresponding error message' do
        input_file = InputFile.new(free_text)
        expect { input_file.parse }.to raise_error('Invalid line')
      end
    end

    context 'with invalid score' do
      it 'raises the corresponding error message' do
        input_file = InputFile.new(invalid)
        expect { input_file.parse }.to raise_error('Invalid pinfalls')
      end
    end

    context 'with invalid number of throwings' do
      it 'raises the corresponding error message' do
        input_file = InputFile.new(invalid)
        expect { input_file.parse }.to raise_error('Invalid pinfalls')
      end
    end
  end
end
