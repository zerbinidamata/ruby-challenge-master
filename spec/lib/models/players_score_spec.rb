require 'models/players_score'

RSpec.describe PlayersScore do
  it 'initialize with a grouped game' do
    game = [{ player: 'Jeff', pinfalls: 10 }]
    players_score = described_class.new(game: game)
    expect(players_score.game).to eq(game.group_by { |obj| obj[:player] })
  end

  it 'compute players score' do
    game = [{ player: 'Jeff', pinfalls: 10 }]
    players_score = described_class.new(game: game)
    expect(players_score.compute_players_score).to eq({ 'Jeff' => [{ :frame => 1, :pinfalls => [10], :total_score=>10 }] })  # rubocop:disable Style/HashSyntax
  end
end
