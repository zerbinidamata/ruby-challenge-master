require 'models/score_board'

def game
  {"Jeff"=>[{:frame=>1, :pinfalls=>[10], :total_score=>20}, {:frame=>2, :pinfalls=>[7, 3], :total_score=>39}, {:frame=>3, :pinfalls=>[9, 0], :total_score=>48}, {:frame=>4, :pinfalls=>[10], :total_score=>66}, {:frame=>5, :pinfalls=>[0, 8], :total_score=>74}, {:frame=>6, :pinfalls=>[8, 2], :total_score=>84}, {:frame=>7, :pinfalls=>[0, 6], :total_score=>90}, {:frame=>8, :pinfalls=>[10], :total_score=>120}, {:frame=>9, :pinfalls=>[10], :total_score=>148}, {:frame=>10, :pinfalls=>[10, 8, 1], :total_score=>166}], "John"=>[{:frame=>1, :pinfalls=>[3, 7], :total_score=>16}, {:frame=>2, :pinfalls=>[6, 3], :total_score=>25}, {:frame=>3, :pinfalls=>[10], :total_score=>44}, {:frame=>4, :pinfalls=>[8, 1], :total_score=>53}, {:frame=>5, :pinfalls=>[10], :total_score=>82}, {:frame=>6, :pinfalls=>[10], :total_score=>101}, {:frame=>7, :pinfalls=>[9, 0], :total_score=>110}, {:frame=>8, :pinfalls=>[7, 3], :total_score=>124}, {:frame=>9, :pinfalls=>[4, 4], :total_score=>132}, {:frame=>10, :pinfalls=>[10, 9, 0], :total_score=>151}]}
end

RSpec.describe ScoreBoard do
  it 'prints the scoreboard' do
    game = [{ player: 'Jeff', pinfalls: 10 }]
    players_score = PlayersScore.new(game: game)
    score = players_score.compute_players_score
    scoreboard = described_class.new(
      total_frames: 10,
      total_pins: 10,
      shots_per_frame: 2,
      final_frame_shots: 3,
      score: score
    )
    scoreboard.compute_scoreboard
    scoreboard = scoreboard.print_scoreboard

    expect(scoreboard[0]).to include('Frame')
    expect(scoreboard[1]).to include('Jeff')
    expect(scoreboard[2]).to include('Pinfalls')
    expect(scoreboard[3]).to include('Score')
  end
end
